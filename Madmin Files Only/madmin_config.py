###########################################################
#                                                         #
# .88b  d88.  .d8b.  d8888b. .88b  d88. d888888b d8b   db #
# 88'YbdP`88 d8' `8b 88  `8D 88'YbdP`88   `88'   888o  88 #
# 88  88  88 88ooo88 88   88 88  88  88    88    88V8o 88 #
# 88  88  88 88~~~88 88   88 88  88  88    88    88 V8o88 #
# 88  88  88 88   88 88  .8D 88  88  88   .88.   88  V888 #
# YP  YP  YP YP   YP Y8888D' YP  YP  YP Y888888P VP   V8P #
#                                                         #
###########################################################

#DEFAULT VALUES: These values will be used after you compile when you start up the server.
class_limits_on  = 0  #Toggles class limits 1= on, 0= off

infantry_limit	 = 16 #Default Infantry limit per team
archer_limit	 = 16 #Default Archer limit per team
cav_limit		 = 16 #Default Cavalry limit per team

dont_autokick	 = 0  	#Toggles autokick for team killing and team wounding 1= players will NOT be auto-kicked, 0= players will be autokicked
dont_kill_horses = 0  	#Toggles automatic stray horse killing 1= stray horses will NOT be auto-killed, 0= stray horses will be auto-killed
tk_limit_default = 3  	#The default of how many teamkills a player can make before they are kicked
tw_limit_default = 250  #The default of how much damage via team wounding a player can accumulate before they are kicked
#KICKING IS NOW IMPLEMENTED (Version 0.6 Beta)

autokick_count_until_autoban = 3 #How many times a player can be autokicked before they are autobanned (temporary) instead.

custom_maps_begin = "scn_multiplayer_maps_end" #The scene representing your first custom map
custom_maps_end   = "scn_multiplayer_maps_end" #Ending scene for your custom maps. YOU SHOULDN'T HAVE TO CHANGE THIS IN MOST SITUATIONS

#Put unique player IDs of players who should have only kick/ban access here.
non_privaledged_admins = []

#Put server messages here. They should be in the format "@My message" (include the quotes) and each should be seperated by a comma
server_messages = ["@Test Message 1", "@Test Message 2", "@Test Message 3"]
server_message_interval = 120 #seconds

#Admin equipment
admin_head_armor = "itm_arena_helmet_yellow"
admin_body_armor = "itm_arena_tunic_yellow"
admin_boots		 = "itm_khergit_leather_boots"
