from header_operations import *
from header_common import *
from madmin_common import *
from madmin_config import *
from header_common import *
from header_operations import *
from module_constants import *
from header_skills import *
from header_mission_templates import *
from header_items import *
from header_terrain_types import *
from header_music import *


adminMessageButton = (0, 0.05, 0, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (player_is_admin, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_u),
       ],
       [
           (start_presentation, "prsnt_madmin_message"),     
       ])
	   
adminArmor = (0, 0, 0, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (player_is_admin, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_i),
       ],
       [
           (multiplayer_send_int_to_server, madmin_server_common, madmin_armor),  
		   (display_message, "@You'll spawn with admin armor next life."),
       ])
	   
adminExp = (0, 0, 0, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (player_is_admin, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_o),
       ],
       [
           (multiplayer_send_int_to_server, madmin_server_common, madmin_explosion),  
       ])
	   
adminFade = (0, 0, 0, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (player_is_admin, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_k),
       ],
       [
           (multiplayer_send_int_to_server, madmin_server_common, madmin_fade),  
       ])
	   
adminCheer = (0, 0, .5, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_m),
       ],
       [
           (multiplayer_send_int_to_server, madmin_server_common, madmin_cheer),  
       ])
	   
adminTaunt = (0, 0, 1.2, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_comma),
       ],
       [
           (multiplayer_send_int_to_server, madmin_server_common, madmin_taunt),  
       ])
	    
madminBanhammer = (0, 0, 0, [
             (eq, "$g_waiting_for_confirmation_to_terminate", 0),
             (multiplayer_get_my_player, ":player_no"),
             (player_is_active, ":player_no"),
			 (player_is_admin, ":player_no"),
			 (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
			 (neg|is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
			 (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
             (key_clicked, key_semicolon),
       ],
       [
           (multiplayer_send_int_to_server, madmin_server_common, madmin_banhammer),  
       ])
	   
mad_agent_hit =	(ti_on_agent_hit, 0, 0, [(eq,1,1)], [
					(store_trigger_param_1, ":haid"), 
					(store_trigger_param_2, ":daid"), #dealer
					(store_trigger_param_3, ":damage"),
					(assign, ":item", reg0),
					
					(try_begin),
						(neg|agent_is_non_player,":haid"),
						(neg|agent_is_non_player,":daid"),
						
						(agent_get_player_id,":hpid",":haid"),
						(agent_get_player_id,":dpid",":daid"),
						
						#banhammer						
						(try_begin),
							(multiplayer_is_server),
							(ge, ":dpid", 0),
							(ge, ":hpid", 0),
							(neq, ":dpid", ":hpid"),
							(player_is_admin, ":dpid"),
							(eq, ":item", "itm_polehammer"),
							(particle_system_burst ,"psys_moon_beam_1", pos0, 60),
							(str_store_player_username,s34,":hpid"),
							(str_store_player_username,s35,":dpid"),
							
							(try_begin),
								(eq, "$m_bhs",0), #KILL
								(agent_deliver_damage_to_agent,":hpid",":hpid",9999),
								(str_store_string, s8, "@Kill"),
							(else_try),
								(eq, "$m_bhs",1), #KICK
								(kick_player, ":hpid"),
								(str_store_string, s8, "@Kick"),
							(else_try),
								(eq, "$m_bhs",2), #TEMP BAN
								(ban_player, ":hpid", 1, ":dpid"),
								(str_store_string, s8, "@Temp Ban"),
							(else_try),
								(eq, "$m_bhs",3), #BAN
								(ban_player, ":hpid", 0, ":dpid"),
								(str_store_string, s8, "@Ban"),
							(try_end),
							
							(server_add_message_to_log, "@{s35} struck {s34} with a banhammer({s8})."),
							(set_trigger_result, 0),
						(try_end),
						
						#teamwounding
						(try_begin),
							(multiplayer_is_server),
							(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
							(ge, ":dpid", 0),
							(ge, ":hpid", 0),
							(player_get_team_no, ":dTeam", ":dpid"),
							(player_get_team_no, ":hTeam", ":hpid"),
							(neq, ":dpid", ":hpid"),
							(eq, ":dTeam", ":hTeam"),
							(player_get_slot,":td",":dpid",madmin_pslt_tw),
							(val_add, ":td", ":damage"),
							(player_set_slot,":dpid",madmin_pslt_tw, ":td"),
							
							(try_begin),
								(ge,":td","$m_tw_limit"),
								(player_set_slot,":dpid",madmin_pslt_tw, 0),
								(server_add_message_to_log, "@{s5} was auto-kicked for teamwounding."),
								#(kick_player, ":dpid"),
								(call_script, "script_mad_kick_or_ban", ":dpid"),
							(try_end),
						(try_end),
					(try_end),	
				])
				
mad_agent_killed = (ti_on_agent_killed_or_wounded, 0, 0, [(eq,1,1),],[
         (store_trigger_param_1, ":daid"),
         (store_trigger_param_2, ":kaid"),
		
		 #Teamkills
		 (try_begin),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
				(agent_is_human, ":daid"),
				(neg|agent_is_non_player, ":daid"),
				(agent_get_player_id, ":dapid", ":daid"),
				(player_get_team_no,":dpTeam", ":dapid"),
				
				(ge, ":kaid", 0),
				(agent_is_human, ":kaid"),
				(neg|agent_is_non_player, ":kaid"),
				(agent_get_player_id, ":kapid", ":kaid"),
				(player_get_team_no, ":kpTeam", ":kapid"),
				
				(try_begin),
					(eq, ":kpTeam", ":dpTeam"),
					(neq, ":kaid", ":daid"),
					(player_get_slot,":tks",":kapid",madmin_pslt_tk),
					(val_add,":tks",1),
					(try_begin),
						(multiplayer_is_server),
						(eq, "$m_autokick_off",0), 
						(ge, ":tks", "$m_tk_limit"),
						
						(str_store_player_username,s5,":kapid"),
						(player_set_slot,":kapid",madmin_pslt_tk,0),
						(server_add_message_to_log, "@{s5} was auto-kicked for teamkilling."),
						#(kick_player, ":kapid"),
						(call_script, "script_mad_kick_or_ban", ":kapid"),
					(else_try),	
						(player_set_slot,":kapid",madmin_pslt_tk,":tks"),
					(try_end),
					
					#Refund killed player
					(player_get_gold, ":gold", ":dapid"),
					(player_get_slot,":refund",":dapid",slot_player_total_equipment_value),
					(val_mul, ":refund", 4),
					(val_div, ":refund", 5),
					(val_add, ":gold", ":refund"), 
					(player_set_gold, ":dapid", ":gold", 0),
				(try_end),
			(try_end),
         ])
		 
mad_after_mission_start = (ti_after_mission_start, 0, 0, [(multiplayer_is_server),], 
		[
			(assign, "$current_server_message", 0),
			#DONE:Moved to game_quick_start via utils
			# (assign, "$m_tk_limit",tk_limit_default), 
			# (assign, "$m_tw_limit",tw_limit_default), 
			# (assign, "$m_autokick_off",dont_autokick), 
			# (assign, "$m_killHorses_off",dont_kill_horses), 
			# (assign, "$m_classLimits_on", class_limits_on),
			# (assign, "$m_infLim_val", infantry_limit),
			# (assign, "$m_arcLim_val", archer_limit),
			# (assign, "$m_cavLim_val", cav_limit),
			#/DONE
			(assign, "$m_t1i", 0),
			(assign, "$m_t1a", 0),
			(assign, "$m_t1c", 0),
			(assign, "$m_t2i", 0),
			(assign, "$m_t2a", 0),
			(assign, "$m_t2c", 0),
			
		])
		
mad_player_joined = (ti_server_player_joined, 1, 0, [(multiplayer_is_server),],
        [
			(store_trigger_param_1, ":player_no"),
			
			(player_set_slot,":player_no",madmin_pslt_tk,0),
			(player_set_slot,":player_no",madmin_pslt_tw,0),
        ])
		
mad_player_joined_delayed = (ti_server_player_joined, 5, 0, [(multiplayer_is_server),],
        [
			(store_trigger_param_1, ":player_no"),
			(store_current_scene,":scene"),
			(val_sub, ":scene", "scn_multi_scene_1"),
			(val_add, ":scene", "str_multi_scene_1"),
			(str_store_string,s7,":scene"),
			(str_store_string, s8, "@Map Name: {s7}"),
			(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message, s8),
        ])
		
mad_escape_pressed = (ti_escape_pressed, 0, 0, [],
		[
			(multiplayer_send_int_to_server, madmin_server_common, madmin_data),  
		])
		
mad_round_ended =  (1, 0, 5, [(multiplayer_is_server),(eq, "$g_round_ended", 1),],
		[
			(get_max_players, ":num_players"),
			(try_for_range, ":cur", 1, ":num_players"),
				(player_is_active, ":cur"),
				(player_set_slot,":cur",madmin_pslt_tk,0),
				(player_set_slot,":cur",madmin_pslt_tw,0),
			(try_end),
		])
		
mad_60_sec = (60, 0, 0, [(multiplayer_is_server),], 
		[
			#Kill horses
			(try_begin),
				(eq, "$m_killHorses_off", 0),
				(try_for_agents,":cur"),
					(agent_is_alive,":cur"),
					(neg|agent_is_human,":cur"),#is a horse
					(agent_get_rider,":rider",":cur"),
					(lt, ":rider", 0),
					(agent_fade_out, ":cur"),
				(try_end),
			(try_end),
		])
		
mad_class_limit_recalc = (30, 0, 0, [(multiplayer_is_server),], 
		[
			(assign, ":t1i", 0),
			(assign, ":t1a", 0),
			(assign, ":t1c", 0),
			(assign, ":t2i", 0),
			(assign, ":t2a", 0),
			(assign, ":t2c", 0),
			
			(get_max_players, ":num_players"),
			(try_for_range, ":cur", 1, ":num_players"),
				(player_is_active, ":cur"),
				
				(player_get_team_no,  ":team", ":cur"),
				(player_get_troop_id, ":troop", ":cur"),
				
				(try_begin),
					(is_between, ":troop", multiplayer_troops_begin, multiplayer_troops_end),
					(call_script, "script_multiplayer_get_troop_class", ":troop"),
					(assign, ":class", reg0),
					
					(try_begin),
						(le, ":class", multi_troop_class_spearman),
						(try_begin),
							(eq, ":team", 0), 
							(val_add, ":t1i", 1),
						(else_try),
							(eq, ":team", 1), 
							(val_add, ":t2i", 1),
						(try_end),
					(else_try),
						(eq, ":class", multi_troop_class_cavalry),
						(try_begin),
							(eq, ":team", 0), 
							(val_add, ":t1c", 1),
						(else_try),
							(eq, ":team", 1), 
							(val_add, ":t2c", 1),
						(try_end),
					(else_try),
						(ge, ":class", multi_troop_class_archer),
						(try_begin),
							(eq, ":team", 0), 
							(val_add, ":t1a", 1),
						(else_try),
							(eq, ":team", 1), 
							(val_add, ":t2a", 1),
						(try_end),
					(try_end),
				(try_end),
			(try_end), #end loop
			
			(assign, "$m_t1i", ":t1i"),
			(assign, "$m_t1a", ":t1a"),
			(assign, "$m_t1c", ":t1c"),
			(assign, "$m_t2i", ":t2i"),
			(assign, "$m_t2a", ":t2a"),
			(assign, "$m_t2c", ":t2c"),
			
			# #DEBUG
			# (assign, reg1,"$m_t1i"),
			# (assign, reg2,"$m_t1a"),
			# (assign, reg3,"$m_t1c"),
			# (assign, reg4,"$m_t2i"),
			# (assign, reg5,"$m_t2a"),
			# (assign, reg6,"$m_t2c"),
			
			# (display_message, "@Class count:^{reg1} {reg2} {reg3}^{reg4} {reg5} {reg6}"),
		])
		
