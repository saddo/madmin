from module_constants import *
from ID_factions import *
from header_items import  *
from header_operations import *
from header_triggers import *

imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced

banhammer = ["polehammer",         "Polehammer", [("pole_hammer",0)], itp_crush_through|itp_type_polearm|itp_no_pick_up_from_ground |itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack, itc_staff,
390 , weight(20)|difficulty(0)|spd_rtng(135) | weapon_length(150)|swing_damage(200 , blunt) | thrust_damage(200 ,  blunt),imodbits_polearm ]


akCount = ["akCount",         "AKCOUNT", [("pole_hammer",0)], itp_crush_through|itp_type_polearm|itp_no_pick_up_from_ground|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_staff,
390 , weight(20)|difficulty(0)|spd_rtng(175) | weapon_length(130)|swing_damage(200 , blunt) | thrust_damage(200 ,  blunt),imodbits_polearm ]
