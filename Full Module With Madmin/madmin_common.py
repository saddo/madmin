from header_common import *

###Madmin Constants###

###Player slots###
madmin_pslt_tk = 101
madmin_pslt_tw = 102

madmin_pslt_changed_teams 	= 103

###Events###
madmin_message 			= 126
madmin_server_common	= 125
madmin_pmAdmin 			= 124
madmin_client_update	= 123

##For madmin_server_common
madmin_explosion 		= 1
madmin_armor 	 		= 2
madmin_fade 	 		= 3
madmin_cheer 	 		= 4
madmin_banhammer 		= 5
madmin_temp_ban	 		= 6
madmin_kill_player 		= 7
madmin_spec_player 		= 8
madmin_swap_player 		= 9
madmin_spec_all 		= 10
madmin_autobalance 		= 11
madmin_data 			= 12
madmin_setting 			= 13
madmin_killStrayHorses	= 14
madmin_taunt			= 15
madmin_heal_player		= 16

##For madmin_client_update and madmin_setting
madmin_tktw				= 1
madmin_tkLimit  		= 2
madmin_twLimit			= 3
madmin_horsekillOff 	= 4
madmin_autokickOff		= 5
madmin_classLimitsOn	= 6
madmin_infLim           = 7
madmin_arcLim           = 8
madmin_cavLim           = 9
madmin_akab				= 10
madmin_bhs				= 11

###For script_mad_kick_or_ban
mad_type_tk = 0
mad_type_tw = 1

###Misc###
ak_count 				= "itm_akCount"